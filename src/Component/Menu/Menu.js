import React from 'react';
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";






const Menu = ({menu, order}) => {
    return (
        <Grid item xs={6}>
            <Paper onClick={order} component={Box} p={2} m={1}>
                {menu.icon}
                <div style = {{display: "inline-block", marginLeft: '10px'}}>
                    <h3 style = {{margin: '0', padding: '0'}}>{menu.name}</h3>
                    {menu.price} KGS
                </div>
            </Paper>
        </Grid>

    );
};

export default Menu;