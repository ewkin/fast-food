import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Grid from "@material-ui/core/Grid";

const OrderBox = ({order, removeOrder}) => {
    let orderPrice = order.price * order.count;
    return (
        <Grid container justify='space-between' alignItems="center" >
            <Grid item>{order.name}   </Grid>
            <Grid item>x {order.count} | {orderPrice} KGS
                <IconButton onClick={() => removeOrder(order.id)} aria-label="delete">
                    <DeleteIcon />
                </IconButton>
            </Grid>
        </Grid>
        );

};

export default OrderBox;