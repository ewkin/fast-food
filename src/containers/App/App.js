import React, {useState} from 'react';
import './App.css';
import RestaurantRoundedIcon from '@material-ui/icons/RestaurantRounded';
import FreeBreakfastRoundedIcon from '@material-ui/icons/FreeBreakfastRounded';
import {nanoid} from 'nanoid';
import {Container} from "@material-ui/core";
import {CssBaseline} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Menu from '../../Component/Menu/Menu';
import OrderBox from "../../Component/OrderBox/OrderBox";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";




const MENU = [
    {name: 'Hamburger', price: 80, icon: <RestaurantRoundedIcon />},
    {name: 'Coffee', price: 70, icon: <FreeBreakfastRoundedIcon/>},
    {name: 'Cheeseburger', price: 90, icon: <RestaurantRoundedIcon /> },
    {name: 'Tea', price: 40, icon: <FreeBreakfastRoundedIcon/>},
    {name: 'Fries', price: 45, icon: <RestaurantRoundedIcon/>},
    {name: 'Coke', price: 50, icon: <FreeBreakfastRoundedIcon/>}
];

const useStyles = makeStyles(theme => ({
    root:{
        marginTop: theme.spacing(2),
    }
}));

const App = () => {
    const classes = useStyles();
    const [orders, setOrders] = useState([]);

    const makeOrder =(order, price) => {
        const index = orders.findIndex(i => i.name===order);
        if(index===-1){
        setOrders([
            ...orders,
            {
                id: nanoid(),
                name: order,
                count: 1,
                price: price
            }]);
        } else {
            let ordersCopy = [...orders];
            let orderCopy = orders[index].count;
            orderCopy++;
            ordersCopy[index].count = orderCopy;
            setOrders(ordersCopy);
        }
    };

    const remove = id =>{
        const index = orders.findIndex(i => i.id===id);
        const ordersCopy = [...orders];
        ordersCopy.splice(index, 1);
        setOrders(ordersCopy);
   }

   const showOrder =() =>{
       let price = orders.reduce((acc, item)=> {
           return acc + item.price*item.count
       },0)
       if (Object.keys(orders).length===0){
           return (
               <div>
                   Order is empty! Please add some items!
               </div>
           )
       } else {
           return (
               <Grid container justify='space-between' alignItems="center" >
                   <Grid item><strong>Total Price:</strong></Grid>
                   <Grid item style={{ paddingRight: 48 }}> {price} KGS</Grid>
               </Grid>
           )
       }
    };



    return (
        <Container maxWidth = 'md' className={classes.root}>
            <CssBaseline/>
            <Grid container spacing = {2} alignItems="stretch" >
                <Grid item xs={5}>
                    <Paper component={Box} height="100%" p={1} m={1} >
                        {orders.map(item => {
                            return(
                                <OrderBox
                                    key = {item.id}
                                    order={item}
                                    removeOrder={remove}
                                />
                            )
                        })}
                        {showOrder()}
                    </Paper>
                </Grid>
                <Grid item xs={7}>
                    <Grid container spacing={1}>
                        {MENU.map((item, index)=>{
                            return(
                                <Menu
                                    key={index}
                                    menu={item}
                                    order={()=>makeOrder(item.name, item.price)}
                                />
                            )
                        }
                    )}
                    </Grid>
                </Grid>
            </Grid>
        </Container>

    );
};

export default App;